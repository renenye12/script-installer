# What is the script installer
The script installer is a tool for the scripts in the SmallScripts collection. When the source is changed, CD/CI automatically builds a installer for every project using it.

# Information
Information on how to use script-installer is located in the Wiki

# License
The project is licensed under GNU GPL v3
