DIR="$(shell pwd)"
SRC="$(DIR)/src/"
BLD="$(DIR)/build.sh"
dist: help
help:
	@echo "* all      Automated (executes build and replace)"
	@echo "* build    Merges source files"
	@echo "* check    Checks for errors in the merged script"
	@echo "* clean    Cleans build files"
	@echo "* replace  Replaces variables that are required for the installation"
	@echo "* info     Shows info to Makefile variables"
all: build
build:
	@echo "-- Merging source files"
	cat $(SRC)/shebang_variables.sh > $(BLD)
	cat $(SRC)/quit.sh >> $(BLD)
	cat $(SRC)/errors.sh >> $(BLD)
	cat $(SRC)/installDone.sh >> $(BLD)
	cat $(SRC)/install.sh >> $(BLD)
	cat $(SRC)/checkDependencies.sh >> $(BLD)
	cat $(SRC)/prepare.sh >> $(BLD)
	cat $(SRC)/info.sh >> $(BLD)
	cat $(SRC)/rootCheck.sh >> $(BLD)
	cat $(SRC)/start.sh >> $(BLD)
	@echo "-- Replacing variables"
	sed "s/\"scinversion\"/\"2\"/g" -i $(BLD)
	@echo "-- Making replace.sh executable"
	chmod +x $(DIR)/replace.sh
check:
	@echo "-- Checking the script using ShellCheck"
	shellcheck --color=always --norc --severity=error $(BLD)
clean:
	@echo "-- Cleaning"
	rm -rf $(BLD)
	chmod -x $(DIR)/replace.sh
replace:
	@echo "Execute replace.sh like so: ./replace.sh \"ApplicationName\" \"ApplicationVersion\" \"InstallDirectory\" \"ZipURL\" \"ZipName\" \"ZipNameWithoutExtension\" \"ChecksumBits\" \"ChecksumURL\" \"RebootRecommendation\""
info:
	@echo DIR=$(DIR)
	@echo SRC=$(SRC)
	@echo BLD=$(BLD)