#!/usr/bin/bash

# 0 BLACK
# 1 RED
# 2 GREEN
# 3 YELLOW
# 4 BLUE
# 5 MAGENTA
# 6 CYAN
# 7 WHITE
#echo "Test: %replace" | sed "s/%replace%/Replaced!/g"
#sed "s/%replace/Replaced!/g" -i file.txt

# Variable declarations
# Static variables (for installation required, may be changed for a different installer)
export scin_static_name=""                                  # Name of the program
export scin_static_version=""                               # Version of the program
export scin_static_directory=""                             # Where the program files are installed to (/etc/<scin_static_name>/ is recommended)
export scin_static_zip=""                                   # The zipfile url
export scin_static_ziptmp=""                                # The zipfile filename
export scin_static_ziptmpne=""                              # The zipfile filename without the .zip extension
export scin_static_checksumbits=""                          # The checksum bits
export scin_static_checksum=""                              # The url containing the checksum. The filename must be zipfilename.sum (.sum might be changed into anything, but that name is recommended.)
export scin_static_rebootrecommendation=""                  # When "true", displays a recommendation to reboot when the installation finished
# Dynamic variables
export scin_dynamic_originaldirectory=""                    # The directory where the user was when the script started
export scin_dynamic_tmp=""                                  # The temporary directory
export scin_dynamic_directoryfile_extractdirectory=""       # The temporary directory where the zip gets extracted to
# Testing variables
export scin_testing_download_regex=""                       # The regex used to identify the text in required downloader program
export scin_testing_download_command=""                     # The command for the required downloader program
export scin_testing_download_commandregex=""                # The command used to identify the required downloader program with the regex
export scin_testing_download_exitcode=""                    # The reported exitcode returned by grep
export scin_testing_download2_regex=""                      # The regex used to identify the text in required downloader program
export scin_testing_download2_command=""                    # The command for the required downloader program (2)
export scin_testing_download2_commandregex=""               # The command used to identify the required downloader program with the regex (2)
export scin_testing_download2_exitcode=""                   # The reported exitcode returned by grep (2)
export scin_testing_general_exitcode=""                     # The reported exitcode used by various things (2)
export scin_testing_unzip_regex=""                          # The regex used to identify the text in required unzip program
export scin_testing_unzip_command=""                        # The command for the required unzip program
export scin_testing_unzip_commandregex=""                   # The command used to identify the required unzip program with the regex
export scin_testing_unzip_exitcode=""                       # The reported exitcode returned by grep
export scin_testing_general_exitcode=""                     # General exitcode used by everything
# SCIN variables
export scin_version=""                                      # Script-Installer version
export scin_install=""                                      # Input of the y/n question
export scin_arg1=""                                         # Argument 1
# Variable values
# Static variables (for installation required)
scin_static_name="projectname"
scin_static_version="projectversion"
scin_static_directory="installdir"
scin_static_zip="archivedownload"
scin_static_ziptmp="ziptemp"
scin_static_ziptmpne="ziptempnoex"
scin_static_checksumbits="shabits"
scin_static_checksum="checksumurl"
scin_static_rebootrecommendation="rebootrcm"
# Dynamic variables
scin_dynamic_originaldirectory="$(pwd)"
scin_dynamic_tmp="/tmp/"
scin_dynamic_directoryfile_extractdirectory="$scin_dynamic_tmp/scin/$scin_static_name/"
# Testing variables
scin_testing_download_regex="GNU Wget "
scin_testing_download_command="/usr/bin/wget"
scin_testing_download_commandregex="$scin_testing_download_command --version"
scin_testing_download_exitcode=""
scin_testing_download2_regex="curl "
scin_testing_download2_command="/usr/bin/curl"
scin_testing_download2_commandregex="$scin_testing_download2_command --version"
scin_testing_download2_exitcode=""
scin_testing_unzip_regex="UnZip "
scin_testing_unzip_command="/usr/bin/unzip"
scin_testing_unzip_commandregex="$scin_testing_download_command --help"
scin_testing_unzip_exitcode=""
scin_testing_general_exitcode=""
# SCIN variables
scin_version="scinversion"
scin_install="false"
scin_arg1="$1"
