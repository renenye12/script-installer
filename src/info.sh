info(){
    echo "script-installer $scin_version"
    echo "created by the StarOpenSource project (https://gitlab.com/staropensource)"
    echo "This project is licensed under GNU GPLv3"
    echo ""
    echo "This program comes with ABSOLUTELY NO WARRANTY."
    echo "This is free software, and you are welcome to redistribute it
    under certain conditions."
    echo ""
    echo "ARGS: $scin_arg1"
    if [ "$scin_arg1" == "-y" ]; then
        echo "Do you want to install \"$scin_static_name\" [y/n]? y"
    else
        echo -n "Do you want to install \"$scin_static_name\" [y/n]? "
        read -r scin_install
        if [ "$scin_install" == "n" ]; then
            quit
        elif [ ! "$scin_install" == "y" ]; then
            echo "Wrong input."
            quitBadly
        fi
    fi
    prepare
}
