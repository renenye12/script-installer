installDone(){
    echo ""
    echo "The installation of $scin_static_name v$scin_static_version finished successfully."
    echo "The original archive is available at $scin_static_directory/original_installerfiles.zip"
    echo "If you want to uninstall this program, go into the directory $scin_static_directory/scin_files/ and execute \"uninstall.sh\" as root"
    if [ "$scin_static_rebootrecommendation" == "true" ]; then
        echo "It is recommended that you reboot your computer."
    fi
    quit
}
