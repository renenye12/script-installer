rootCheck(){
    if [ "$(whoami)" == "root" ]; then
        info
    else
        echo "This installer must be executed as root"
        exit 1
    fi
}
