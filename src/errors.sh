cdError(){
    tput setaf 1
    echo -n "!"
    tput setaf 3
    echo -n "> "
    tput setaf 1
    echo "Failed to cd into a directory."
    quitBadly
}
