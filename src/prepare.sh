prepare(){
    tput setaf 3
    echo -n "=> "
    tput setaf 6
    echo "Preparing installation"
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Checking dependencies (phase one)"
    dependenciesOne
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Checking dependencies (phase two)"
    dependenciesTwo
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Creating required filepaths"
    tput setaf 5
    mkdir --parents $scin_dynamic_directoryfile_extractdirectory
    scin_testing_general_exitcode="$?"
    if [ ! "$scin_testing_general_exitcode" == "0" ]; then
        tput setaf 1
        echo -n "!"
        tput setaf 3
        echo -n "> "
        tput setaf 1
        echo "Failed to create filepath $scin_dynamic_directoryfile_extractdirectory"
        quitBadly
    fi
    tput setaf 5
    mkdir --parents $scin_static_directory
    scin_testing_general_exitcode="$?"
    if [ ! "$scin_testing_general_exitcode" == "0" ]; then
        tput setaf 1
        echo -n "!"
        tput setaf 3
        echo -n "> "
        tput setaf 1
        echo "Failed to create filepath $scin_static_directory"
        quitBadly
    fi
    tput setaf 5
    cd $scin_dynamic_directoryfile_extractdirectory || cdError
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Downloading install package"
    tput setaf 5
    $scin_testing_download_command --quiet --no-dns-cache $scin_static_zip --output-document $scin_dynamic_directoryfile_extractdirectory/$scin_static_ziptmp
    scin_testing_general_exitcode="$?"
    if [ ! "$scin_testing_general_exitcode" == "0" ]; then
        tput setaf 1
        echo -n "!"
        tput setaf 3
        echo -n "> "
        tput setaf 1
        echo "Failed to download archive"
        quitBadly
    fi
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Checking archive integrity"
    if [ "$scin_static_checksumbits" == "1" ] || [ "$scin_static_checksumbits" == "224" ] || [ "$scin_static_checksumbits" == "256" ] || [ "$scin_static_checksumbits" == "384" ] || [ "$scin_static_checksumbits" == "512" ]; then
    tput setaf 5
        $scin_testing_download2_command $scin_static_checksum | /usr/bin/sha"$scin_static_checksumbits"sum --check
        scin_testing_general_exitcode="$?"
        if [ ! "$scin_testing_general_exitcode" == "0" ]; then
            tput setaf 1
            echo -n "!"
            tput setaf 3
            echo -n "> "
            tput setaf 1
            echo "Invalid checksum"
            quitBadly
        fi
    else
        tput setaf 1
        echo -n "!"
        tput setaf 3
        echo -n "> "
        tput setaf 1
        echo "Wrong checksum bits defined"
        quitBadly
    fi
    install
}
