install(){
    tput setaf 3
    echo -n "=> "
    tput setaf 6
    echo "Starting installation"
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Decompressing"
    tput setaf 5
    $scin_testing_unzip_command -qq -d .. -u $scin_static_ziptmpne
    scin_testing_general_exitcode="$?"
    if [ ! "$scin_testing_general_exitcode" == "0" ]; then
        tput setaf 1
        echo -n "!"
        tput setaf 3
        echo -n "> "
        tput setaf 1
        echo "Failed to decompress archive"
        quitBadly
    fi
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Checking archive version"
    if [ ! -f "$scin_dynamic_directoryfile_extractdirectory/scin_files/version" ]; then
        tput setaf 1
        echo -n "!"
        tput setaf 3
        echo -n "> "
        tput setaf 1
        echo "Archive version information not found"
        quitBadly
    else
        if [ ! "$(cat $scin_dynamic_directoryfile_extractdirectory/scin_files/version)" == "$scin_version" ]; then
            tput setaf 1
            echo -n "!"
            tput setaf 3
            echo -n "> "
            tput setaf 1
            echo "Invalid archive version"
            quitBadly
        fi
    fi
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Creating archive backup"
    tput setaf 5
    mv $scin_dynamic_directoryfile_extractdirectory/$scin_static_ziptmp $scin_dynamic_directoryfile_extractdirectory/original_installerfiles.zip
    tput setaf 5
    cd scin_files || cdError
    tput setaf 5
    chmod +x ./*
    tput setaf 3
    bash
    echo -n "> "
    tput setaf 6
    echo "Starting pre-move installation phase"
    tput setaf 5
    ./install "$scin_version" "-s0" "$scin_static_directory" "$scin_dynamic_directoryfile_extractdirectory" "$scin_static_name" "$scin_static_version"
    tput setaf 5
    cd $scin_dynamic_directoryfile_extractdirectory/.. || cdError
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Moving temporary files to install directory"
    tput setaf 5
    mv $scin_dynamic_directoryfile_extractdirectory/* $scin_static_directory --force
    tput setaf 5
    cd $scin_static_directory/scin_files || cdError
    tput setaf 3
    echo -n "> "
    tput setaf 6
    echo "Starting mid installation phase"
    tput setaf 5
    ./install "$scin_version" "-s1" "$scin_static_directory" "$scin_dynamic_directoryfile_extractdirectory" "$scin_static_name" "$scin_static_version"
    installDone
}
