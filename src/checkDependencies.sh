dependenciesOne(){
    tput setaf 5
    $scin_testing_download_commandregex | grep "$scin_testing_download_regex" &> /dev/null
    scin_testing_download_exitcode="$?"
    if [ ! "$scin_testing_download_exitcode" == "0" ]; then
        tput setaf 1
        tput setaf 3
        echo -n "> "
        tput setaf 1
        echo "Wget is not installed to $scin_testing_download_command"
        quitBadly
    fi
    tput setaf 5
    $scin_testing_download2_commandregex | grep "$scin_testing_download2_regex" &> /dev/null
    scin_testing_download2_exitcode="$?"
    if [ ! "$scin_testing_download2_exitcode" == "0" ]; then
        tput setaf 1
        echo -n "!"
        tput setaf 3
        echo -n "> "
        tput setaf 1
        echo "cURL is not installed to $scin_testing_download2_command"
        quitBadly
    fi
    tput setaf 5
    $scin_testing_unzip_commandregex | grep "$scin_testing_download_regex" &> /dev/null
    scin_testing_unzip_exitcode="$?"
    if [ ! "$scin_testing_unzip_exitcode" == "0" ]; then
        tput setaf 1
        echo -n "!"
        tput setaf 3
        echo -n "> "
        tput setaf 1
        echo "Unzip is not installed to $scin_testing_unzip_command"
        quitBadly
    fi
}
dependenciesTwo(){
    tput setaf 1
    echo -n "!"
    tput setaf 3
    echo -n "> "
    tput setaf 1
    echo "Task \"Checking dependencies (phase two)\" failed."
    echo "   Function not implemented."
}
