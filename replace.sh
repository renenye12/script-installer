#!/usr/bin/bash
if [ ! "$1" == "" ] && [ ! "$2" == "" ] && [ ! "$3" == "" ] && [ ! "$3" == "" ] && [ ! "$4" == "" ] && [ ! "$5" == "" ] && [ ! "$6" == "" ] && [ ! "$7" == "" ] && [ ! "$8" == "" ] && [ ! "$9" == "" ]; then
    if [ "$9" == "true" ] || [ "$9" == "false" ]; then
        if [ "$7" == "1" ] || [ "$7" == "224" ] || [ "$7" == "256" ] || [ "$7" == "384" ] || [ "$7" == "512" ]; then
            echo -n "Replacing... "
	        sed "s:\"projectname\":\"$1\":g" -i "$(pwd)"/build.sh
	        sed "s:\"projectversion\":\"$2\":g" -i "$(pwd)"/build.sh
	        sed "s:\"installdir\":\"$3\":g" -i "$(pwd)"/build.sh
	        sed "s:\"archivedownload\":\"$4\":g" -i "$(pwd)"/build.sh
	        sed "s:\"ziptemp\":\"$5\":g" -i "$(pwd)"/build.sh
	        sed "s:\"ziptempnoex\":\"$6\":g" -i "$(pwd)"/build.sh
	        sed "s:\"shabits\":\"$7\":g" -i "$(pwd)"/build.sh
	        sed "s:\"checksumurl\":\"$8\":g" -i "$(pwd)"/build.sh
	        sed "s:\"rebootrcm\":\"$9\":g" -i "$(pwd)"/build.sh
            echo "Done"
            exit 0
        else
            echo "ERROR: The shabits variable must be \"1\", \"224\", \"256\", \"384\" or \"512\"."
            exit 3
        fi
    else
        echo "ERROR: The reboot variable must be \"true\" or \"false\"."
        exit 2
    fi
else
    echo "ERROR: At least one argument wasn't passed."
    exit 1
fi
